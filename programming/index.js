require("dotenv").config();
require("./config/passport");
const db = require("./models");
const express = require("express");
const cors = require("cors")
const app = express();

const authRoutes = require("./routes/auth")
const adminRoutes = require("./routes/admin")
const userRoutes = require("./routes/user")

//Setup Cross Origin / Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routes
app.use("/auth", authRoutes)
app.use("/admin", adminRoutes)
app.use("/user", userRoutes)

// Running Server
app.listen(process.env.PORT, () => {
    console.log("Server listening on port " + process.env.PORT);
});

// Connect Database
db.sequelize.sync({ force: true }).then(() => {
    console.log("Completed Connect And Sync");
});
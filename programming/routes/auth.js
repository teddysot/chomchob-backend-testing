const passport = require("passport");
const { login, register } = require("../controllers/auth");
const router = require("express").Router();

const auth = passport.authenticate("jwt-auth", { session: false });

router.post("/login", login);
router.post("/register", register);

module.exports = router;
const passport = require("passport");
const { createWallet, transfer } = require("../controllers/user");
const router = require("express").Router();

const auth = passport.authenticate("jwt-auth", { session: false });

router.post('/createWallet', auth, createWallet)
router.post('/transfer', auth, transfer)

module.exports = router;
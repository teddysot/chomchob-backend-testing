const passport = require("passport");
const {
    addCurrency,
    increaseBalance,
    decreaseBalance,
    getAllBalanceByUser,
    adjustExchangeRate,
    addExchange
} = require("../controllers/admin");
const router = require("express").Router();

const auth = passport.authenticate("jwt-auth", { session: false });


router.post("/addcurrency", auth, addCurrency)
router.post("/addexchange", auth, addExchange)
router.patch("/increasebalance", auth, increaseBalance)
router.patch("/decreasebalance", auth, decreaseBalance)
router.patch("/adjustexchangerate", auth, adjustExchangeRate)
router.get("/getallbalance/:id", auth, getAllBalanceByUser)

module.exports = router;
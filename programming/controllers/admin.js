const db = require("../models");

const addCurrency = async (req, res) => {

    if (!isAdmin(req.user.role)) {
        res.status(400).send({ message: "Unauthorized" })
        return
    }
    const { name, abbreviation } = req.body
    const targetCurrency = await db.Currency.findOne({ where: { name } })

    if (targetCurrency) {
        res.status(400).send({ message: "Currency already existed." })
    }
    else {
        await db.Currency.create({
            name,
            abbreviation
        })

        res.status(201).send({ message: "Currency created." })
    }
}

const increaseBalance = async (req, res) => {

    if (!isAdmin(req.user.role)) {
        res.status(400).send("Unauthorized")
        return
    }

    const { address, amount } = req.body

    const targetWallet = await db.Wallet.findOne({ where: { address } })
    if (targetWallet) {
        targetWallet.update({ balance: targetWallet.balance + amount })
        res.status(201).send({ message: "Increase balance success" })
    }
    else {
        res.status(400).send({ message: "Wallet is not exist" })
    }
}

const decreaseBalance = async (req, res) => {

    if (!isAdmin(req.user.role)) {
        res.status(400).send("Unauthorized")
        return
    }

    const { address, amount } = req.body

    const targetWallet = await db.Wallet.findOne({ where: { address } })
    if (targetWallet) {
        targetWallet.update({ balance: targetWallet.balance - amount })
        res.status(201).send({ message: "Decrease balance success" })
    }
    else {
        res.status(400).send({ message: "Wallet is not exist" })
    }
}

const getAllBalanceByUser = async (req, res) => {

    if (!isAdmin(req.user.role)) {
        res.status(400).send("Unauthorized")
        return
    }

    const { id } = req.params

    let targetWallets = await db.User.findAll({
        include: [
            {
                model: db.Wallet,
                required: true,
                include: {
                    model: db.Currency
                }
            },
        ],
        where: { id }
    })

    if (targetWallets) {
        res.status(201).send({ message: "Retrieved wallets success", targetWallets })
    }
    else {
        res.status(400).send({ message: "ID is not exist" })
    }
}

const addExchange = async (req, res) => {
    const { fromCurrency, destCurrency, rate } = req.body

    const fromCur = await db.Currency.findOne({ where: { abbreviation: fromCurrency }, attributes: ["id"] })

    if (!fromCur) {
        res.status(400).send({ message: `${fromCurrency} is not exist` })
        return
    }

    const destCur = await db.Currency.findOne({ where: { abbreviation: destCurrency }, attributes: ["id"] })

    if (!destCur) {
        res.status(400).send({ message: `${destCurrency} is not exist` })
        return
    }

    const targetExchange = await db.Exchange.findOne({
        where: {
            from_cur_id: fromCur.id,
            dest_cur_id: destCur.id
        }
    })

    if (!targetExchange) {
        await db.Exchange.create({
            rate,
            from_cur_id: fromCur.id,
            dest_cur_id: destCur.id
        })
        res.status(201).send({ message: "Exchange rate added" })
    }
    else {
        res.status(400).send({ message: "Exchange rate existed" })
    }
}

const adjustExchangeRate = async (req, res) => {
    const { fromCurrency, destCurrency, rate } = req.body

    const fromCur = await db.Currency.findOne({ where: { abbreviation: fromCurrency }, attributes: ["id"] })
    const destCur = await db.Currency.findOne({ where: { abbreviation: destCurrency }, attributes: ["id"] })

    const targetExchange = await db.Exchange.findOne({
        where: {
            from_cur_id: fromCur.id,
            dest_cur_id: destCur.id
        }
    })

    if (targetExchange) {
        targetExchange.update({ rate })
        res.status(201).send({ message: "Exchange rate updated" })
    }
    else {
        res.status(400).send({ message: "Currencies pair not found" })
    }
}

const isAdmin = (role) => {
    return role === "Admin" ? true : false
}

module.exports = {
    addCurrency,
    increaseBalance,
    decreaseBalance,
    getAllBalanceByUser,
    adjustExchangeRate,
    addExchange
};
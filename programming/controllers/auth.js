const db = require("../models");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const register = async (req, res) => {
    const { username, password, email, role } = req.body;
    const targetUser = await db.User.findOne({ where: { username } });

    if (targetUser) {
        res.status(400).send({ message: "Username already taken." });
    } else {
        const salt = bcrypt.genSaltSync(Number(process.env.SALT_ROUND));
        const hashedPW = bcrypt.hashSync(password, salt);

        await db.User.create({
            username,
            email,
            role,
            password: hashedPW
        });

        res.status(201).send({ message: "User created." });
    }
};

const login = async (req, res) => {
    const { username, password } = req.body;
    const targetUser = await db.User.findOne({ where: { username } });
    if (targetUser) {
        if (bcrypt.compareSync(password, targetUser.password)) {
            const token = jwt.sign({
                id: targetUser.id,
                username: targetUser.username,
                email: targetUser.email,
                role: targetUser.role,
            }, process.env.SECRET_KEY, { expiresIn: 3600 });
            res.status(200).send({
                token,
                id: targetUser.id,
                username: targetUser.username,
                email: targetUser.email,
                role: targetUser.role,
            });
        } else {
            res.status(400).send({ message: "Username or password incorrect." });
        }
    } else {
        res.status(400).send({ message: "Username or password incorrect." });
    }
};

module.exports = {
    register,
    login,
};
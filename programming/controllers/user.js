const { nanoid } = require("nanoid");
const db = require("../models");

const createWallet = async (req, res) => {
    const { id } = req.user
    const { currency } = req.body

    const targetCurrency = await db.Currency.findOne({ where: { abbreviation: currency } })
    const foundWallet = await db.Wallet.findOne({ where: { user_id: id, currency_id: targetCurrency.id } })

    if (foundWallet) {
        res.status(400).send({
            message: `${currency} wallet existed`
        })

        return
    }

    let targetWallet = null
    let newAddress = null
    do {
        newAddress = nanoid(30)
        targetWallet = await db.Wallet.findOne({ where: { address: newAddress } })

        if (!targetWallet) {
            break;
        }
    } while (targetWallet)

    if (targetCurrency) {
        const wallet = await db.Wallet.create({
            address: newAddress,
            balance: 0,
            user_id: id,
            currency_id: targetCurrency.id
        })

        res.status(200).send({ wallet })
    }
    else {
        res.status(400).send({ message: "Currency not found" })
    }
}

const transfer = async (req, res) => {
    const { id } = req.user
    const { currency, targetAddress, amount } = req.body

    const userCurrency = await db.Currency.findOne({ where: { abbreviation: currency } })
    if (!userCurrency) {
        res.status(400).send({ message: "Currency not found" })
        return
    }

    const userWallet = await db.Wallet.findOne({ where: { user_id: id, currency_id: userCurrency.id } })
    if (!userWallet) {
        res.status(400).send({ message: `${currency} Wallet not found` })
        return
    }

    const targetWallet = await db.Wallet.findOne({ where: { address: targetAddress } })
    if (!targetWallet) {
        res.status(400).send({ message: "Target Wallet not found" })
        return
    }

    if (targetWallet.currency_id !== userCurrency.id) {
        const exchangeRate = await db.Exchange.findOne({ where: { from_cur_id: userCurrency.id, dest_cur_id: targetWallet.currency_id } })
        if (!exchangeRate) {
            res.status(400).send({ message: "Exchange rate not found" })
            return
        }

        userWallet.update({ balance: userWallet.balance - amount })
        targetWallet.update({ balance: targetWallet.balance + (amount * exchangeRate.rate) })

        await db.Transaction.create({
            amount: amount * exchangeRate.rate,
            type: "transfer",
            owner_wallet_id: userWallet.id,
            dest_wallet_id: targetWallet.id
        })
        res.status(200).send({ message: "Transfer success" })
    }
    else {
        userWallet.update({ balance: userWallet.balance - amount })
        targetWallet.update({ balance: targetWallet.balance + amount })

        await db.Transaction.create({
            amount: amount,
            type: "transfer",
            owner_wallet_id: userWallet.id,
            dest_wallet_id: targetWallet.id
        })
        res.status(200).send({ message: "Transfer success" })
    }

}

module.exports = {
    createWallet,
    transfer
};
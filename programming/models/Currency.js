module.exports = (sequelize, DataTypes) => {
    const Currency = sequelize.define("Currency", {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      abbreviation: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
    }, {
      tableName: "Currencies",
      timestamps: false
    });

    Currency.associate = models => {
        Currency.hasMany(models.Wallet, { foreignKey: "currency_id" });
        Currency.hasMany(models.Exchange, { foreignKey: "from_cur_id" });
        Currency.hasMany(models.Exchange, { foreignKey: "dest_cur_id" });
    };
  
    return Currency;
  }
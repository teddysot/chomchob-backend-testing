module.exports = (sequelize, DataTypes) => {
    const Transaction = sequelize.define("Transaction", {
      amount: {
        type: DataTypes.DOUBLE,
        allowNull: false
      },
      type: {
        type: DataTypes.STRING,
        allowNull: false
      },
    }, {
      tableName: "Transactions",
      timestamps: true
    });

    Transaction.associate = models => {
        Transaction.belongsTo(models.Wallet, { foreignKey: "owner_wallet_id" });
        Transaction.belongsTo(models.Wallet, { foreignKey: "dest_wallet_id" });
    };
  
    return Transaction;
  }
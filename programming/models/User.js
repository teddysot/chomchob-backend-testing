module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define("User", {
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    role: {
      type: DataTypes.ENUM("Customer", "Admin"),
      allowNull: false
    },
  }, {
    tableName: "Users",
    timestamps: false
  });

  User.associate = models => {
    User.hasMany(models.Wallet, { foreignKey: "user_id" });
  };

  return User;
}
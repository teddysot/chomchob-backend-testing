module.exports = (sequelize, DataTypes) => {
    const Exchange = sequelize.define("Exchange", {
        rate: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
    }, {
        tableName: "Exchanges",
        timestamps: false
    });

    Exchange.associate = models => {
        Exchange.belongsTo(models.Currency, { foreignKey: "from_cur_id" });
        Exchange.belongsTo(models.Currency, { foreignKey: "dest_cur_id" });
    };

    return Exchange;
}
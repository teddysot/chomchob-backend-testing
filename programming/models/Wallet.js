module.exports = (sequelize, DataTypes) => {
    const Wallet = sequelize.define("Wallet", {
      address: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      balance: {
        type: DataTypes.DOUBLE,
        allowNull: false
      },
    }, {
      tableName: "Wallets",
      timestamps: false
    });

    Wallet.associate = models => {
        Wallet.belongsTo(models.User, { foreignKey: "user_id" });
        Wallet.belongsTo(models.Currency, { foreignKey: "currency_id" });
        Wallet.hasMany(models.Transaction, { foreignKey: "owner_wallet_id" });
        Wallet.hasMany(models.Transaction, { foreignKey: "dest_wallet_id" });
    };
  
    return Wallet;
  }
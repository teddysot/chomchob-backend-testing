module.exports = (sequelize, DataTypes) => {
    const ItemCode = sequelize.define("ItemCode", {
      code: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
    }, {
      tableName: "ItemCodes",
      timestamps: false
    });

    ItemCode.associate = models => {
        ItemCode.belongsTo(models.Item, { foreignKey: "item_id" });
    };
  
    return ItemCode;
  }
module.exports = (sequelize, DataTypes) => {
    const BundleCode = sequelize.define("BundleCode", {
      code: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
    }, {
      tableName: "BundleCodes",
      timestamps: false
    });

    BundleCode.associate = models => {
        BundleCode.belongsTo(models.Bundle, { foreignKey: "bundle_id" });
    };
  
    return BundleCode;
  }
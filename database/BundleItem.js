module.exports = (sequelize, DataTypes) => {
    const BundleItem = sequelize.define("BundleItem", {
      quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
    }, {
      tableName: "BundleItems",
      timestamps: false
    });

    BundleItem.associate = models => {
        BundleItem.belongsTo(models.Item, { foreignKey: "item_id" });
        BundleItem.belongsTo(models.Bundle, { foreignKey: "bundle_id" });
    };
  
    return BundleItem;
  }
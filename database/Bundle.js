module.exports = (sequelize, DataTypes) => {
    const Bundle = sequelize.define("Bundle", {
        bundle_name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        bundle_detail: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sale_price: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        open_sale_date: {
            type: DataTypes.DATE,
        },
        end_sale_date: {
            type: DataTypes.DATE,
        },
        discount_price: {
            type: DataTypes.DOUBLE,
        },
        start_discount_price: {
            type: DataTypes.DATE,
        },
        end_discount_price: {
            type: DataTypes.DATE,
        },
    }, {
        tableName: "Bundles",
        timestamps: false
    });

    Bundle.associate = models => {
        Bundle.hasMany(models.Item, { foreignKey: "bundle_id" });
        Bundle.hasMany(models.BundleCode, { foreignKey: "bundle_id" });
    };

    return Bundle;
}
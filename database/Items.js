module.exports = (sequelize, DataTypes) => {
    const Item = sequelize.define("Item", {
      product_name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      product_detail: {
        type: DataTypes.STRING,
        allowNull: false
      },
      sale_price: {
        type: DataTypes.DOUBLE,
        allowNull: false
      },
      open_sale_date: {
        type: DataTypes.DATE,
      },
      end_sale_date: {
        type: DataTypes.DATE,
      },
      discount_price: {
        type: DataTypes.DOUBLE,
      },
      start_discount_price: {
        type: DataTypes.DATE,
      },
      end_discount_price: {
        type: DataTypes.DATE,
      },
    }, {
      tableName: "Items",
      timestamps: false
    });

    Item.associate = models => {
        Item.hasMany(models.ItemCode, { foreignKey: "item_id" });
        Item.hasMany(models.BundleItem, { foreignKey: "item_id" });
    };
  
    return Item;
  }
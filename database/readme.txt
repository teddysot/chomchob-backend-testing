Items table
- I have open/end sale/discount date in the schema because when I query I can check that is the item still on sale or it has any discount.
ItemCodes table
- I have item_id which ref from Items table so that I will know what is this itemcode contains and quantity for how many pieces.

Bundles table
- Similar to Items table. I want to seperate them because I will have BundleItems table for telling what are the items that bundle contains
BundleItems table
- I have bundle_id and item_id that ref from Items and Bundles tables which will tell what each bundles have which items and how many pieces.
BundleCodes table
- Similar to ItemCodes table that has a bundle_id ref to Bundles table, so I will know that code are from which bundle and how many user will received.